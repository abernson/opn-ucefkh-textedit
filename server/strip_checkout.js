Meteor.methods({
    stripMembershipCheckout: function(token, user) {
        var email = user.emails[0].address;
        var options = {card: token.id, plan: "opn-coffee-membership", email: email};
        var Stripe = StripeAPI(Meteor.settings.STRIPE_SECRET_KEY);

        //Test API Key
        //var Stripe = StripeAPI('sk_test_S3KfxjO8AEdzwbJCklAJhfHd');

        var stripeCustomersCreateSync = Meteor.wrapAsync(Stripe.customers.create, Stripe.customers);
        var response = stripeCustomersCreateSync(options);

        if (response) {
            Meteor.users.update({_id:user._id}, {$set: {isGuest: false, stripe: {id: response.id}}});
        }
        return response;
    }
});
