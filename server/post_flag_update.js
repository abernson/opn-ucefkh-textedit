Meteor.methods({
    flagPost: function (postId, title, userId) {
        Posts.update({_id:postId}, {$set:{flagged:true}});
        sendFlaggedEmail(postId, title, userId);
    },
    unFlagPost: function (postId) {
        Posts.update({_id:postId}, {$set:{flagged:false}});
    }
});