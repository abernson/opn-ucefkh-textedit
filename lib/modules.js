Telescope.modules.add("postMeta", {
    template: "post_flag_link",
    order: 30
});

Telescope.modules.add("postHeading", {
    template: "post_flag_text",
    order: 1
});


Telescope.modules.add("profileEdit", {
    template: "edit_profile_purchase_button",
    order: 1
});
